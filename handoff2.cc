/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//
// Default network topology includes some number of AP nodes specified by
// the variable nWifis (defaults to two).  Off of each AP node, there are some
// number of STA nodes specified by the variable nStas (defaults to two).
// Each AP talks to its associated STA nodes.  There are bridge net devices
// on each AP node that bridge the whole thing into one network.
//
//      +-----+
//      | STA |
//      +-----+
//    192.168.0.4  --------------------------->
//      --------   <---------------------------
//      WIFI STA
//      --------------------------+
//        ((*))                   |
//                                |
//              ((*))             |             ((*))
//             -------                         -------
//             WIFI AP   CSMA ========= CSMA   WIFI AP
//             -------   ----           ----   -------
//             ##############           ##############
//                 BRIDGE                   BRIDGE
//             ##############           ##############
//               192.168.0.2              192.168.0.3
//               +---------+              +---------+
//               | AP Node |              | AP Node |
//               +---------+--------------+---------+
//                                |
//                          192.168.0.1
//                       +--------------+
//                       |    Server    |
//                       +--------------+
//
#include "ns3/log.h"
#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/bridge-helper.h"
#include "ns3/propagation-module.h"
#include "ns3/gtk-config-store.h"
#include <vector>
#include <stdint.h>
#include <sstream>
#include <fstream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("handoff2");

FlowMonitorHelper flowmon;
Ptr<FlowMonitor> monitor = NULL;
void startMon() {
	if (!monitor) return;
	monitor->CheckForLostPackets();

	Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier>(
			flowmon.GetClassifier());
	std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats();
	for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i =
			stats.begin(); i != stats.end(); ++i) {
		Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow(i->first);
		if ((t.sourceAddress == "192.168.0.4"
				&& t.destinationAddress == "192.168.0.1")) {
			std::cout << "Flow " << i->first << " (" << t.sourceAddress
					<< " -> " << t.destinationAddress << ")\n";
			std::cout << "  Tx Bytes:   " << i->second.txBytes << "\n";
			std::cout << "  Rx Bytes:   " << i->second.rxBytes << "\n";
			std::cout << "  Throughput: "
					<< i->second.rxBytes * 8.0
							/ (i->second.timeLastRxPacket.GetSeconds()
									- i->second.timeFirstTxPacket.GetSeconds())
							/ 1024 / 1024 << " Mbps\n";
		}
	}
}

Ptr<WifiNetDevice> mwifiDev;
void LinkUp() {
	bool newAP = false;
	static bool stationIsUp = true;
	static int apCount = 0;
	static Mac48Address cBssid = Mac48Address();
	if (mwifiDev) {
		stationIsUp = mwifiDev->IsLinkUp();
		Ptr<WifiMac> mac = mwifiDev->GetMac();
		Ptr<RegularWifiMac> rmac = mac->GetObject<RegularWifiMac>();
		if (rmac && rmac->GetBssid() != cBssid) {
			cBssid = rmac->GetBssid();
			newAP = true;
			apCount++;
		}
	} else {
		stationIsUp = !stationIsUp;
	}
	Time t_now = Simulator::Now();
	double f_milli = 1.0e-6 * t_now.GetNanoSeconds();
	NS_LOG_FUNCTION(
			"station is " << ((stationIsUp) ? "UP" : "DOWN") << " with " << ((newAP) ? "new" : "old") << " AP at " << f_milli);
	if (newAP) {
		monitor->StopRightNow();
		monitor->StartRightNow();
		startMon();
	}
}

void sinkReceive(const Ptr<const Packet> packet, const Address &address) {
	static Time revT = Simulator::Now();
	NS_LOG_LOGIC("Received " << packet->GetSize() << " from " << address);
	Time delta = (Simulator::Now() - revT);
	std::cout << delta.GetMilliSeconds() << std::endl;
	revT = Simulator::Now();
}

void changeDirection(Ptr<ConstantVelocityMobilityModel> mob, Time next,
		double velX) {
	mob->SetVelocity(Vector(-velX, 0, 0));
	Simulator::Schedule(next, &changeDirection, mob, next, -velX);
}

int main(int argc, char *argv[]) {

	//int maxChannel = 11;
	uint32_t nWifis = 2;
	bool writeMobility = false;
	double velX = 0.5, velY = 0;//, rssi_th = RSSI_THRESHOLD;
	double startApp = 0.5, endApp = 60.0;

	CommandLine cmd;
	cmd.AddValue("nWifis", "Number of wifi networks", nWifis);
	cmd.AddValue("writeMobility", "Write mobility trace", writeMobility);
	cmd.AddValue("velX", "Horizontal velocity of the station", velX);
	cmd.AddValue("velY", "Vertical velocity of the station", velY);
	cmd.AddValue("startApp", "Start time for application", startApp);
	cmd.AddValue("endApp", "End time for application", endApp);
	cmd.Parse(argc, argv);

	LogComponentEnable("handoff2", LOG_LEVEL_INFO);
	//LogComponentEnable("ApWifiMac", LOG_LEVEL_FUNCTION);
	//LogComponentEnable("StaWifiMac", LOG_LEVEL_INFO);
	//LogComponentEnable("OnOffApplication", LOG_LEVEL_INFO);

	NodeContainer backboneNodes;
	NetDeviceContainer backboneDevices;
	//Ipv4InterfaceContainer backboneInterfaces;
	std::vector<NodeContainer> staNodes;
	std::vector<NetDeviceContainer> staDevices;
	std::vector<NetDeviceContainer> apDevices;
	std::vector<Ipv4InterfaceContainer> staInterfaces;
	std::vector<Ipv4InterfaceContainer> apInterfaces;

	InternetStackHelper stack;
	CsmaHelper csma;
	Ipv4AddressHelper ip;
	Ptr<CsmaChannel> csmaChannel = CreateObjectWithAttributes<CsmaChannel>(
			"DataRate", StringValue("100Mbps"), "Delay", StringValue("2ms"));
	ip.SetBase("192.168.0.0", "255.255.255.0");

	backboneNodes.Create(nWifis);
	stack.Install(backboneNodes);

	backboneDevices = csma.Install(backboneNodes, csmaChannel);
	NodeContainer serverNode;
	serverNode.Create(1);
	stack.Install(serverNode);
	NetDeviceContainer serverDev = csma.Install(serverNode, csmaChannel);
	Ipv4InterfaceContainer svrInterface;
	svrInterface = ip.Assign(serverDev);
	double wifiX = 10.0, wifiY = -20.0, deltaX = 60, staHeight = 20;

	//!< Wifi default configuration
	std::string phyMode("DsssRate5_5Mbps");
	WifiHelper wifi = WifiHelper::Default();
	wifi.SetStandard(WIFI_PHY_STANDARD_80211b);

	YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
	wifiPhy.SetPcapDataLinkType(YansWifiPhyHelper::DLT_IEEE802_11_RADIO); //!< Support radiotap capture
	YansWifiChannelHelper wifiChannel;
	//!< reference loss must be changed since 802.11b is operating at 2.4GHz
	wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
	wifiChannel.AddPropagationLoss("ns3::LogDistancePropagationLossModel",
			"Exponent", DoubleValue(3.0), //!< office path loss exponent
			"ReferenceLoss", DoubleValue(40.0459));
	wifiPhy.SetChannel(wifiChannel.Create());
	//!< Add a non-QoS upper mac, and disable rate control
	NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default();
	wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager", "DataMode",
			StringValue(phyMode), "ControlMode", StringValue(phyMode));
	Ssid staSSID = Ssid("ns3Handoff");

	for (uint32_t i = 0; i < nWifis; ++i) {
		NetDeviceContainer apDev;
		Ipv4InterfaceContainer apInterface;
		MobilityHelper mobility;
		BridgeHelper bridge;

		ListPositionAllocator mLPA;
		mLPA.Add(Vector(wifiX, wifiY, 0));
		mobility.SetPositionAllocator(&mLPA);

		// setup the AP.
		mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
		mobility.Install(backboneNodes.Get(i));
		wifiMac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(staSSID)); //!< No SSID is set
		apDev = wifi.Install(wifiPhy, wifiMac, backboneNodes.Get(i));

		// Change channel of the next AP
		//int FreqChannel = 1 + i * ((maxChannel + 1) / nWifis);
		//if (i != 0) apDev.Get(0)->GetObject<WifiNetDevice>()->GetPhy()->SetChannelNumber(
		//		FreqChannel);
		NetDeviceContainer bridgeDev;
		bridgeDev = bridge.Install(backboneNodes.Get(i),
				NetDeviceContainer(apDev, backboneDevices.Get(i)));

		// assign AP IP address to bridge, not wifi
		apInterface = ip.Assign(backboneDevices.Get(i));
		apDevices.push_back(apDev);
		apInterfaces.push_back(apInterface);

		// decide new location
		wifiX += deltaX;
	}

	// setup the STA
	NodeContainer sta;
	NetDeviceContainer staDev;
	MobilityHelper mobility;
	Ipv4InterfaceContainer staInterface;
	sta.Create(1);
	stack.Install(sta);
	mobility.SetMobilityModel("ns3::ConstantVelocityMobilityModel");
	//!< Place the station above the first AP
	ListPositionAllocator mLPA;
	Vector staVec(wifiX - (nWifis * deltaX), wifiY - staHeight, 0);
	mLPA.Add(staVec);
	mobility.SetPositionAllocator(&mLPA);
	// set station velocity
	mobility.Install(sta);
	Ptr<ConstantVelocityMobilityModel> Stamobility;
	Stamobility = sta.Get(0)->GetObject<ConstantVelocityMobilityModel>();
	Stamobility->SetVelocity(Vector(velX, velY, 0));
	Simulator::Schedule(Seconds(deltaX / velX), &changeDirection, Stamobility,
			Seconds(deltaX / velX), velX);

	wifiMac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(staSSID),
			"ActiveProbing", BooleanValue(false));
	staDev = wifi.Install(wifiPhy, wifiMac, sta);
	staInterface = ip.Assign(staDev);
	/*
	 Ptr<WifiNetDevice> wifiNetDevice =
	 staDev.Get(0)->GetObject<WifiNetDevice>();
	 mwifiDev = wifiNetDevice;
	 if (wifiNetDevice) wifiNetDevice->AddLinkChangeCallback(
	 MakeCallback(&LinkUp));
	 else NS_FATAL_ERROR("WifiNetDevice is NULL");
	 */
	staDevices.push_back(staDev);
	staInterfaces.push_back(staInterface);

	// save everything in containers.
	staNodes.push_back(sta);

	/*
	 // Setup 2nd STA
	 NodeContainer sta2;
	 NetDeviceContainer staDev2;
	 MobilityHelper mobility2;
	 Ipv4InterfaceContainer staInterface2;
	 sta2.Create(1);
	 stack.Install(sta2);
	 mobility2.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	 ListPositionAllocator mLPA2;
	 mLPA2.Add(
	 Vector(wifiX - (nWifis * deltaX) + deltaX, wifiY - staHeight - 5,
	 0));
	 mobility2.SetPositionAllocator(&mLPA2);
	 // set station velocity
	 mobility2.Install(sta2);
	 Ssid staSSID2 = Ssid("Client-00:00:00:00:00:07");
	 wifiMac.SetType("ns3::StaWifiMac2", "Ssid", SsidValue(staSSID2),
	 "ActiveProbing", BooleanValue(true));
	 staDev2 = wifi.Install(wifiPhy, wifiMac, sta2);
	 staInterface2 = ip.Assign(staDev2);
	 Ptr<WifiNetDevice> wifiNetDevice2 =
	 staDev.Get(0)->GetObject<WifiNetDevice>();
	 wifiNetDevice2->GetPhy()->SetChannelNumber(7);

	 //staDevices.push_back(staDev2);
	 //staInterfaces.push_back(staInterface2);

	 // save everything in containers.
	 //staNodes.push_back(sta2);
	 */
	//std::string InterAPProtocol = "ns3::TcpSocketFactory";
	std::string StaServerProtocol = "ns3::UdpSocketFactory";

	/* Inter AP Messages * /
	int localPort = 12329;
	for (unsigned int i = 0; i < nWifis; i++) {
		InetSocketAddress addr1 = InetSocketAddress(
				apInterfaces[i].GetAddress(0), localPort);
		Ptr<McvapApp> mcvapp = CreateObject<McvapApp>();
		for (uint32_t j = 0; j < apInterfaces.size(); j++)
			mcvapp->AddNeighbor(InetSocketAddress(apInterfaces[j].GetAddress(0),
			AP_MSG_PORT));
		int channel = 1 + i * ((maxChannel + 1) / nWifis);
		mcvapp->Setup(TcpSocketFactory::GetTypeId(), addr1, channel);
		backboneNodes.Get(i)->AddApplication(mcvapp); // added to AP1
		mcvapp->SetStartTime(Seconds(startApp));
		mcvapp->SetStopTime(Seconds(endApp));
	}
	/ * Station-Server comm start */
	// sender (station)
	Address svrAddress;
	int StaServerPort = 50491;
	svrAddress = InetSocketAddress(svrInterface.GetAddress(0), StaServerPort);
	OnOffHelper onoff = OnOffHelper(StaServerProtocol, svrAddress);
	onoff.SetConstantRate(DataRate("65kb/s"));
	//onoff.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
	onoff.SetAttribute("OffTime",
			StringValue("ns3::ConstantRandomVariable[Constant=0]"));
	onoff.SetAttribute("PacketSize", UintegerValue(172)); //!< Voice payload size of G.711	+ RTP header(12 bytes)

	ApplicationContainer udpApps = onoff.Install(sta.Get(0)); // added to STA
	udpApps.Start(Seconds(startApp));
	udpApps.Stop(Seconds(endApp));

	// Receiver (Server)
	PacketSinkHelper svrSink(StaServerProtocol,
			InetSocketAddress(Ipv4Address::GetAny(), StaServerPort));
	udpApps = svrSink.Install(serverNode.Get(0)); // added to SERVER
	serverNode.Get(0)->GetApplication(0)->TraceConnectWithoutContext("Rx",
			MakeCallback(&sinkReceive));
	udpApps.Start(Seconds(startApp));
	udpApps.Stop(Seconds(endApp));
	/* Station-server comm end */

	// Calculate Throughput using Flowmonitor
	monitor = flowmon.InstallAll();

	wifiPhy.EnablePcap("wifi-bridgingap1", apDevices[0], false);
	wifiPhy.EnablePcap("wifi-bridgingap2", apDevices[1], false);
	wifiPhy.EnablePcap("wifi-bridgingsta", staDev, false);
	//wifiPhy.EnablePcap("wifi-bridgingsta2", staDev2);
	csma.EnablePcap("csma-server", serverDev, true);
	csma.EnablePcap("csmaAp", backboneDevices, true);

	Packet::EnablePrinting();

	if (writeMobility) {
		AsciiTraceHelper ascii;
		MobilityHelper::EnableAsciiAll(
				ascii.CreateFileStream("wifi-wired-bridging.mob"));
	}

	//GtkConfigStore config;
	//config.ConfigureAttributes();
	Simulator::Stop(Seconds(11.0));
	Simulator::Run();
	Simulator::Destroy();
}
