#include "apClientManagero.h"
#include "ns3/simulator.h"
#include "ns3/assert.h"
#include "ns3/log.h"
#include "ns3/tag.h"
#include "ns3/boolean.h"
#include "ns3/double.h"
#include "ns3/uinteger.h"
#include "ns3/wifi-phy.h"
#include "ns3/wifi-mac.h"
#include "ns3/trace-source-accessor.h"
#include "wifi-mac-header.h"
#include "wifi-mac-trailer.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("apClientManagero");

NS_OBJECT_ENSURE_REGISTERED(apClientManagero);

TypeId apClientManagero::GetTypeId(void) {
	static TypeId tid = TypeId("ns3::apClientManagero").SetParent<Object>();
	return tid;
}

apClientManagero::apClientManagero() {
	m_stations = std::vector<p_client>();
}

p_client apClientManagero::createStation(Mac48Address macAddr) {
	VAPWifiRemoteStation *cSta = new VAPWifiRemoteStation();
	cSta->m_state = VAPWifiRemoteStation::BRAND_N;
	cSta->m_macaddress = macAddr;
	cSta->rssi = -200;
	cSta->per = 1;
	cSta->csa_count = 0;
	cSta->will_handoff = false;
	cSta->new_rssi = -200;
	cSta->new_channel = 0;
	cSta->new_per = 1.0;
	cSta->new_num = 1;
	return cSta;
}
;
void apClientManagero::deleteStation(Mac48Address macAddr) {
	if(m_stations.size() < 1) {
		NS_LOG_LOGIC("Empty station list");
		return;
	}
	for (stationList::iterator i = m_stations.begin(); i != m_stations.end();) {
		if ((*i)->m_macaddress == macAddr) {
			m_stations.erase(i);
		} else i++;
	}
}

void apClientManagero::updateRssiInfo(Mac48Address macAddr, int rssi) {
	p_client cSta = searchStation(macAddr);
	if (cSta)
		cSta->rssi = rssi;
}

void apClientManagero::assocStation(Mac48Address macAddr) {
	p_client cSta = searchStation(macAddr);
	if (!cSta)
		cSta = addStation(macAddr);
	cSta->m_state = VAPWifiRemoteStation::GOT_ASSOC_T_OK;
}

p_client apClientManagero::addStation(Mac48Address macAddr) {
	p_client cSta = searchStation(macAddr);
	if (!cSta) {
		cSta = createStation(macAddr);
		m_stations.push_back(cSta);
	}
	return cSta;
}

p_client apClientManagero::searchStation(Mac48Address macAddr) {
	if(m_stations.size() < 1) return NULL;
	for (stationList::const_iterator i = m_stations.begin();
			i != m_stations.end(); i++) {
		if ((*i)->m_macaddress == macAddr) {
			return (*i);
		}
	}
	return NULL;
}

p_client apClientManagero::Get(unsigned int i) {
	if (i >= m_stations.size())
		return NULL;
	return m_stations.at(i);
}

} // namepace ns3
