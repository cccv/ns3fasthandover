#ifndef AP_STATION_MANAGER_H
#define AP_STATION_MANAGER_H
#include <vector>
#include <utility>
#include "ns3/mac48-address.h"
#include "ns3/traced-callback.h"
#include "ns3/packet.h"
#include "ns3/object.h"
#include "ns3/nstime.h"
#include "wifi-mode.h"
#include "wifi-tx-vector.h"
#include "ht-capabilities.h"

namespace ns3 {

struct VAPWifiRemoteStation;
typedef struct VAPWifiRemoteStation * p_client;
typedef std::vector<VAPWifiRemoteStation *> stationList;

class apClientManagero: Object {
public:
	static TypeId GetTypeId(void);
	apClientManagero();
	p_client createStation(Mac48Address);
	void deleteStation(Mac48Address);
	void updateRssiInfo(Mac48Address, int);
	void assocStation(Mac48Address);
	p_client addStation(Mac48Address);
	p_client searchStation(Mac48Address);
	p_client Get(unsigned int);

	stationList m_stations;
};

/**
 * A struct that holds information about each remote station.
 */
struct VAPWifiRemoteStation {
	/**
	 * State of the station
	 */
	enum {
		BRAND_N, DISASSOC, WAIT_ASSOC_TX_OK, GOT_ASSOC_T_OK
	} m_state;

	Mac48Address m_macaddress;  //!< Mac48Address of the remote station
	Mac48Address m_bssid;  //!< bssid of the remote station
	Ipv4Address m_ipaddress; //!< IP adress of the client
	Ipv4Address m_apIpaddress; //!< IP adress of the current AP
	double rssi; //!< RSSI value
	double per; //!< packet error rate
	int channel; //!< current channel
	bool will_handoff; //!< true if client is set for handoff

	//!< channel switch announcement
	double new_rssi;
	double new_per;
	int new_num; //! new AP station count
	int new_channel;
	int csa_count;
	Ipv4Address new_ipaddress;
};

}
#endif
